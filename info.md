```
air

+------------------------------------------------------------------------------------------+
|                                          Disks                                           |
+----------+--------+-------------+-----------+--------+------------------+----------------+
|  Device  |  Size  | Unallocated | Interface | Vendor |      Model       |   Serial No.   |
+----------+--------+-------------+-----------+--------+------------------+----------------+
| /dev/sda | 234 GB |             |   SATA    |        | APPLE SSD SM256C | S0N8NEAB312663 |
+----------+--------+-------------+-----------+--------+------------------+----------------+
           | 234 GB |             |
           +--------+-------------+

+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
|                                                                                                Disk Partitions                                                                                                 |
+------------------+-----------+---------+---------+---------+-------------------------------------------------------+-------------+--------------------------------------+--------------------------------------+
|       Disk       |  Device   |  Size   |  Free   | FS type |                      Mount point                      |    Label    |                 UUID                 |            Partition type            |
+------------------+-----------+---------+---------+---------+-------------------------------------------------------+-------------+--------------------------------------+--------------------------------------+
| APPLE SSD SM256C | /dev/sda1 | 0.20 GB |         | vfat    |                                                       | EFI         | 67E3-17ED                            | GPT: EFI system                      |
|                  | /dev/sda2 |   55 GB |         | hfsplus |                                                       | OSX         | 4f3bcdf6-6372-3651-b753-1a080287395e | 53746f72-6167-11aa-aa11-00306543ecac |
|                  | /dev/sda3 | 0.61 GB |         | hfsplus |                                                       | Recovery HD | 2dcd178e-ffb6-38e3-8dbe-d80046f0b78e | 426f6f74-0000-11aa-aa11-00306543ecac |
|                  | /dev/sda4 | 0.20 GB | 0.18 GB | hfsplus | /boot/efi                                             | boot-efi    | 0ecd5f4b-f646-3185-9b22-40f4ebbb5212 | 48465300-0000-11aa-aa11-00306543ecac |
|                  | /dev/sda5 | 1.00 GB | 0.79 GB | ext4    | /boot                                                 | boot        | a42e289d-ebc5-4b91-ac6e-1a5ec4f862fd | GPT: Linux                           |
|                  | /dev/sda6 |  122 GB |         | LUKS    | /dev/mapper/luks-f42b3991-bd43-4ee4-a18d-66478274e3ab |             | f42b3991-bd43-4ee4-a18d-66478274e3ab | GPT: Linux                           |
|                  |           |         |   50 GB | ext4    | /data                                                 | data        | 9a7d63e5-7c22-463a-9cbd-0b6faaa725e3 |                                      |
|                  | /dev/sda7 |   50 GB |         | LUKS    | /dev/mapper/luks-bcf6570a-ddd8-420f-ab9e-effd09817ba8 |             | bcf6570a-ddd8-420f-ab9e-effd09817ba8 | GPT: Linux                           |
|                  |           |         |   34 GB | ext4    | /                                                     | root        | 05e0032a-38ac-4933-9c97-a8d250e3a8e1 |                                      |
|                  | /dev/sda8 |  3.9 GB |         | swap    | [SWAP]                                                | swap        | ad51a277-fb3a-42a2-88ed-3de8ae130ccb | GPT: SWAP                            |
+------------------+-----------+---------+---------+---------+-------------------------------------------------------+-------------+--------------------------------------+--------------------------------------+
                               |  234 GB |   85 GB |
                               +---------+---------+


#
# /etc/fstab
# Created by anaconda on Sun May  7 14:11:38 2017
#
# Accessible filesystems, by reference, are maintained under '/dev/disk'
# See man pages fstab(5), findfs(8), mount(8) and/or blkid(8) for more info
#
/dev/mapper/luks-bcf6570a-ddd8-420f-ab9e-effd09817ba8 /                       ext4    defaults,x-systemd.device-timeout=0 1 1
UUID=a42e289d-ebc5-4b91-ac6e-1a5ec4f862fd /boot                   ext4    defaults        1 2
UUID=0ecd5f4b-f646-3185-9b22-40f4ebbb5212 /boot/efi               hfsplus defaults        0 2
/dev/mapper/luks-f42b3991-bd43-4ee4-a18d-66478274e3ab /data                   ext4    defaults,x-systemd.device-timeout=0 1 2
UUID=ad51a277-fb3a-42a2-88ed-3de8ae130ccb swap                    swap    defaults        0 0

+------------+---------------+------+--------------+----------+-------------------+
|   Device   |  IP address   | Name | Description  |  Driver  |        MAC        |
+------------+---------------+------+--------------+----------+-------------------+
| wlp2s0b1   | 192.168.10.9  | wifi | Onboard WiFi | brcmsmac | 04:0c:ce:db:10:a4 |
+------------+---------------+------+--------------+----------+-------------------+
| lo         | 127.0.0.1     |      |              |          | 00:00:00:00:00:00 |
| virbr0     | 192.168.122.1 |      |              | bridge   | 52:54:00:30:45:c6 |
| virbr0-nic |               |      |              | tun      | 52:54:00:30:45:c6 |
+------------+---------------+------+--------------+----------+-------------------+

# Generated by NetworkManager
search linuts.org linuts.com
nameserver 208.67.222.222
nameserver 208.67.220.220

Kernel IP routing table
Destination     Gateway         Genmask         Flags Metric Ref    Use Iface
0.0.0.0         192.168.10.8    0.0.0.0         UG    600    0        0 wlp2s0b1
192.168.10.0    0.0.0.0         255.255.255.0   U     600    0        0 wlp2s0b1
192.168.122.0   0.0.0.0         255.255.255.0   U     0      0        0 virbr0
```
